# ChatGPT 镜像站汇总

## 汇总所知道的 ChatGPT 镜像站（国内正常网络下就能访问）

### 介绍

可免费直接使用的资源现在越来越少了，且用且珍惜。

所有内容均由大家共同创建, 若发现链接失效或分类不符合标题, 请及时提 issue 进行纠正。

无效的链接在每次作者核验后将被移除， **最后核验日期：2024-05-24** 。

众人拾柴火焰高，欢迎大家一起贡献，请直接提交 issue 或者 pr （只能提交普通网络下就可以访问成功的网址唷。）


### 无需登录直接可用的：
- https://chat18.aichatos.xyz/ - 免费可用
- https://chat.ttext.cn/ - 免费时限制每小时6次提问
- https://www.zxf7460.cn/home - GPT-3.5-16k免费使用，吐字块，限制每日3次数，不需要登陆


### 登录后可以免费使用的：
- https://chat.aidutu.cn - 微信扫码登录后免费可用 
- https://openai.itedus.cn/ - 关注公众号后免费可用
- https://home.xiamis.xyz - 需要登录
- https://free.easychat.work - 支持ddg搜索以及google bard，需要登录使用 gpt-3.5-turbo gpt-3.5-turbo-16k
- https://ai.pushplus.plus - 需要登录，会员10元/月无限制。
- https://codesdevs.github.io - 扫码一键登录，4.0和mj绘画
- http://chat.apeto.cn/ - 需要登录
- https://ai.yunweikc.com/ - 需要登录
- https://gezhe.com/ - 需要登录（AI一键生成PPT）
- http://www.xckfsq.com/index/chatgpt - 需要登录，信创开放平台
- http://chat.612ch.com/ - 需注册登录，智能咨询小天使(GPT3.5 每日免费提问20次不限Token)
- http://s.suolj.com - 微信扫码一键登录，支持4.0，支持对话和midjourney画图
- https://so.csdn.net/so/ai - CSDN和外部合作伙伴联合研发的生成式AI产品(每天2次免费提问)
- https://chatsnext.goldgom.top 邮箱注册，GPT3.5免费，GPT4按量计费或者VIP 支持AI绘画，联网搜索 特别棒！
- https://chat.7jm.cn/index.html - 微信扫码登录后免费可用（新增于2024-03-26）
- https://gptpanda.com.cn/- 需注册登录（新增于2024-03-26）
- https://gpt.91chat-ai.cn/  - 需要登录
- https://c6.a0.chat/#/web/chat  - 需要登录


### 国内自研大模型汇总：
- https://yiyan.baidu.com/ - 文心一言，百度出品
- https://tongyi.aliyun.com/ - 通义千问，阿里出品
- https://techday.sensetime.com/ - SenseTime，商汤科技出品
- https://tiangong.kunlun.com/ - 天工AI助手，昆仑万维集团
- https://xinghuo.xfyun.cn/ - 星火认知大模型，讯飞
- https://moss.fastnlp.top/ - Moss，复旦团队出品
- https://www.so.com/zt/invite.html - 360智脑，360出品


### AI工具集导航网站:
- https://hao.94c.cn/ - 好导航就是c
- https://ai-bot.cn/ - 第一次加载较慢
- https://www.aiyjs.com/ - AI工具集导航网站
- https://www.aigc.cn/ - AIGC 导航网站
- https://www.ainav.net/ - AI工具集导航网站
- https://c.runoob.com/ai/ - 菜鸟工具
- https://d0424.ai55.cc/ - 公众号:赞美青春
- https://www.html5iq.com/hottopic/y6yVS7MFRipoBh8QnK0G1 - gpt镜像站搜集
- https://gitee.com/oschina/awesome-llm - 这可能是最全的开源 LLM （大语言模型）整理


### 开源模型（可自行部署到本地的）：
- https://github.com/Chanzhaoyu/chatgpt-web - 支持 access_token 和 apikey 两种方式
- https://github.com/Yidadaa/ChatGPT-Next-Web - 可以不下载到本地直接云部署 
- https://gitee.com/aniu-666/chat-gpt-website
- https://github.com/hncboy/chatgpt-web-java
- http://be.apeto.cn/archives/shang-ye-ban-chatgpt - 商业可部署chatgpt项目(演示地址:http://chat.apeto.cn)
- https://gitee.com/lboot/lucy-chat


### 插件：
- https://www.wetab.link/zh/ - Edge、Chrome 浏览器插件（现已开始收费，8.8一月）
- https://chatgpt.gold/zh/ - 在 idea 上使用 chatgpt
- https://bito.ai/ - bito插件(idea插件/vscode插件/chrome插件均支持) 注册登录即可免费使用，必须联网使用，外网较慢


### 需要付费的：
- https://chatai.yxboot.com/ - 云行 AI 


### 国外其它大模型汇总：
- https://gemini.google.com/ - 谷歌出品，需魔法。
- https://slack.com - 使用流程[参考](https://mp.weixin.qq.com/s/XECDWPv3CRrHfWmoxGLTvg)
- https://h2o.ai/ - 国外开源可访问模型(可以使用中文回复,当前时间无法理解上下文)


### 其它多模态 AI 技术：
- https://github.com/CompVis/stable-diffusion - Stable Diffusion，AI 画图 [ 开源 ]
- https://github.com/getcursor/cursor - Cursor，使用Ai来辅助编程 [ 开源 ]
- https://writesonic.com/ - AI 帮你文案策划
- https://yige.baidu.com/ - 文心一格，AI 作图，百度出品
- https://www.midjourney.com/ - Midjourney，AI 画图
- https://gamma.app/ - Gamma，AI 帮你生成 PPT
- https://fliki.ai/ - fliki 通过文本自动生成AI配音的视频
- https://chatbot.js.cn - AI小助理、AI 绘画



[参考](https://github.com/click33/chatgpt---mirror-station-summary)
